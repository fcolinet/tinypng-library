# README #

Cette librairie, pour CodeIgniter v2, permet une utilisation du service fournis par [tinypng.com](https://tinypng.com/).
Afin d'utiliser cette librairie, il est nécessaire de créer une clé API. [tinypng.com](https://tinypng.com/) propose une offre gratuite pour 500 conversions par mois [ici](https://tinypng.com/developers).

### Installation ###

Copier / coller le dossier 'application' dans CodeIgniter.

Editer le fichier 'application\config\tinypng.php'

* Mettre le paramètre 'tinyPNG_enable' à TRUE
* Renseigner la clé API dans 'tinyPNG_api_key'

Editer le fichier 'application\config\autoload.php'

* Dans '$autoload['libraries']', ajouter 'TinyPNG'
* Dans '$autoload['config']', ajouter 'tinypng'

### Utilisation ###

```
#!php
<?php
try {
	$this->tinypng->shrink("/chemin/absolu/de/la/source/img.png", "/chemin/absolu/de/la/destination/img.png");
} catch(Exception $ex) {
	var_dump($ex->getMessage());
	exit();
}
```