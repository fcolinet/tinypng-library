<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| tinyPNG parameters
|--------------------------------------------------------------------------
|
*/
$config['tinyPNG_enable']	= FALSE;
$config['tinyPNG_api_url']	= 'https://api.tinypng.com/shrink';
$config['tinyPNG_api_user']	= 'api';
$config['tinyPNG_api_key']	= '';