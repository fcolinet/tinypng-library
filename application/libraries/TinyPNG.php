<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter tinyPNG Class
 *
 * Permits to shrink a png file.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Florent Colinet
 */
class CI_TinyPNG {

	var $tinyPNG_enable		= "";
	var $tinyPNG_api_url	= "";
	var $tinyPNG_api_user	= "";
	var $tinyPNG_api_key	= "";
	var $CI;

	/**
	 * Constructor - Sets tinyPNG Preferences
	 *
	 * The constructor can be passed an array of config values
	 */
	public function __construct($config = array())
	{
		log_message('debug', "TinyPNG Class Initialized");

		// Set the super object to a local variable for use throughout the class
		$this->CI =& get_instance();

		foreach (array('tinyPNG_enable', 'tinyPNG_api_url', 'tinyPNG_api_user', 'tinyPNG_api_key') as $key)
		{
			$this->$key = (isset($config[$key])) ? $config[$key] : $this->CI->config->item($key);
		}

		log_message('debug', "TinyPNG routines successfully run");
	}

	// --------------------------------------------------------------------

	/**
	 * Shrink
	 *
	 * @access	public
	 * @return	void
	 */
	function shrink($input, $output='')
	{
		if(!$this->tinyPNG_api_user) {
			log_message('debug', "TinyPNG isn't enabled.");
			throw new Exception("TinyPNG isn't enabled.");
		}

		if(file_exists($input))
			$input = file_get_contents($input);
		else
			$input = '';

		$request = curl_init();

		curl_setopt_array($request, array(
			CURLOPT_URL => $this->tinyPNG_api_url,
			CURLOPT_USERPWD => $this->tinyPNG_api_user . ":" . $this->tinyPNG_api_key,
			CURLOPT_POSTFIELDS => $input,
			CURLOPT_BINARYTRANSFER => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => true,

			/*
				Uncomment below if you have trouble validating our SSL certificate.
				Download cacert.pem from: http://curl.haxx.se/ca/cacert.pem
			*/
			// CURLOPT_CAINFO => __DIR__ . "/cacert.pem",

			CURLOPT_SSL_VERIFYPEER => true
		));

		$response = curl_exec($request);

		if(curl_getinfo($request, CURLINFO_HTTP_CODE) === 201) {

			$json = json_decode(substr($response, curl_getinfo($request, CURLINFO_HEADER_SIZE), curl_getinfo($request, CURLINFO_CONTENT_LENGTH_DOWNLOAD)));
			log_message('debug', "TinyPNG shrink success : " . curl_getinfo($request, CURLINFO_HTTP_CODE) . " -> " . $json->input->size . " -> " . $json->output->size . " : " . $json->output->ratio);

			/* Compression was successful, retrieve output from Location header. */
			$headers = substr($response, 0, curl_getinfo($request, CURLINFO_HEADER_SIZE));
			foreach(explode("\r\n", $headers) as $header) {
				if(substr($header, 0, 10) === "Location: ") {
					$request = curl_init();
					curl_setopt_array($request, array(
						CURLOPT_URL => substr($header, 10),
						CURLOPT_RETURNTRANSFER => true,

						/* Uncomment below if you have trouble validating our SSL certificate. */
						// CURLOPT_CAINFO => __DIR__ . "/cacert.pem",

						CURLOPT_SSL_VERIFYPEER => true
					));

					if($output != '') return file_put_contents($output, curl_exec($request));
					else return curl_exec($request);

				}
			}
		} else {
			$json = json_decode(substr($response, curl_getinfo($request, CURLINFO_HEADER_SIZE), curl_getinfo($request, CURLINFO_CONTENT_LENGTH_DOWNLOAD)));
			log_message('debug', "TinyPNG shrink error : " . curl_getinfo($request, CURLINFO_HTTP_CODE) . " -> " . $json->error . " -> " . $json->message);
			throw new Exception($json->message);
		}
	}

}
// END CI_TinyPNG class

/* End of file TinyPNG.php */
/* Location: ./application/libraries/TinyPNG.php */
